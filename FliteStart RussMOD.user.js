// ==UserScript==
// @name         FliteStart RussMOD
// @namespace    http://tampermonkey.net/
// @version      0.3.0
// @description  try to take over the world!
// @author       Russell Stout
// @match        http*://www.flitestart.com/*
// @match        http*://flitestart.com/*

// @grant        GM_addStyle
/* globals jQuery, $, dayjs */
// @require      http://code.jquery.com/jquery-3.4.1.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.17/dayjs.min.js

// ==/UserScript==

(function() {
  'use strict';

  /***********************************
   * MAIN NAV
   ***********************************/
  let mainNav = $('<div id="main-nav"></div>');
  let mainNavHtml = `
    <a href="http://www.flitestart.com/FliteStart.aspx?company=1">Reservations</a>
    <a href="http://www.flitestart.com/Profile.aspx?company=1">My Profile</a>
    <a href="http://www.flitestart.com/print.aspx?logon=StoutR&company=1">My Schedule</a>
    <a href="https://aviationweather.gov/adds/metars/index?submit=1&station_ids=KSNA&chk_metars=on&hoursStr=2&std_trans=translated&chk_tafs=on" target="_blank">METAR KSNA</a>
    <a href="https://e6bx.com/weather/KSNA/?showDecoded=1" target="_blank">METAR 2 KSNA</a>
    <a href="https://www.wunderground.com/forecast/KSNA" target="_blank">WU KSNA</a>
    <a href="https://skyvector.com/?ll=33.939258473943724,-118.31753539659461&chart=301&zoom=5" target="_blank">SkyVector</a>
  `;
  mainNav.html(mainNavHtml);
  $('body').prepend(mainNav);

  /***********************************
   * HOME PAGE
   ***********************************/

  let schedTable = $('.schedule');

  // TODO: Find cleaner way to find index for Chris
  let headerRow = schedTable.find('tr')[0];
  let $headerRowCells = $(headerRow).find('td');
  let onClickStrings = $headerRowCells.map(function(index, elem) {
    return $(this).attr('onclick') || 'empty';
  }).get();
  /*
  let chrisIndex = onClickStrings.findIndex((ocString) => {
    return ocString.includes('LewisC');
  });
  */

  // Create a list of column indexes that will NOT be hidden
  let allowedIndexes = [
    0,
    // chrisIndex,
  ];

  // Find SportStar indexes
  $headerRowCells.each(function() {
    let $this = $(this);
    if ($this.hasClass('AT1')
      || $this.hasClass('AT2')
      || $this.hasClass('AT3')
      || $this.hasClass('AT4')
      || $this.hasClass('AT5')
      || $this.hasClass('AT6')
    ) {
      allowedIndexes.push($this.index());
    }
  });

  schedTable.find('tr td').each(function () {
    let $this = $(this);

    // Add hidden-cell class to all cells not in allowed list
    const cellIndex = $this.index();
    if (allowedIndexes.indexOf(cellIndex) === -1) {
      $this.addClass('hidden-cell');
    }
  });

  // Add next / previous buttons for navigating
  // Get current date
  let currentDateString = $('#txtDate').attr('formateddate');
  let dayjsDate = dayjs(currentDateString); // get & set

  let goToRelativeDate = function(dayjsObj, relativeIndex) {
    let newDate = dayjsDate.add(1, 'day');
    if (relativeIndex < 0) {
      newDate = dayjsDate.subtract(1, 'day');
    }
    return goToDate(newDate);
  };

  let goToDate = function(dayjsObj) {
    let dateInUrl = dayjsObj.format('dddd%2c+MMMM+D%2c+YYYY');
    window.location.href = `http://www.flitestart.com/FliteStart.aspx?date=${dateInUrl}&company=1`;
    return false;
  };

  // Insert next / previous buttons
  let nextPrevButtonDiv = $('<div class="next-prev-buttons"></div>');
  let prevButton = $('<button type="button">Previous</button>').click(function() {
    return goToRelativeDate(dayjsDate, -1);
  });
  let nextButton = $('<button type="button">Next</button>').click(function() {
    return goToRelativeDate(dayjsDate, 1);
  });
  let toggleCFIs = $('<button type="button">Toggle CFIs</button>').click(function() {
    $('table#fg').toggleClass('hide-cells');
  });
  schedTable.after(nextPrevButtonDiv);
  nextPrevButtonDiv.append(prevButton);
  nextPrevButtonDiv.append(toggleCFIs);
  nextPrevButtonDiv.append(nextButton);
  // Hide CFIs by default
  $('table#fg').addClass('hide-cells');

  // Check for my reservation on that day
  let checkForBookingByFormattedDate = function($td) {
    let dateString = $td.attr('title');
    let newDateString = dateString
      .replace(',', '%2c')
      .replace(' 0', '+')
      .replace(' ', '+');

    $.get( `//www.flitestart.com/FliteStart.aspx?date=${newDateString}&company=1`, function( htmlAsString ) {
      if (htmlAsString.includes('StoutR')) {
        $td.attr('client', 'StoutR');
      }
    });
  };

  let highlightAllBookedDates = function() {
    $('#RadCalendar1_Top').find('td').each(function() {
      checkForBookingByFormattedDate($(this));
    });
  };
/*
  This button is no longer necessary; dates are highlighted every time the page is loaded
  let highlightButton = $('<button>Highlight Dates</button>').click(function() {
    highlightAllBookedDates();
    return false;
  });
  $('#RadCalendar1_wrapper').parent().append(highlightButton);
*/
  /***********************************
   * SCHEDULE PAGE
  ***********************************/

  let today = new Date();

  // check how many days in a month code from https://dzone.com/articles/determining-number-days-month
  function daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
  }

  function createCalendar(month, year, scheduledDates) {

    let firstDay = (new Date(year, month)).getDay();
    let numberOfDaysInMonth = daysInMonth(month, year);
    // body of the calendar
    let calendarTable = $('<table></table>');
    let headingRow = $("<tr></tr>");
    let headingCell = $("<th colspan='7'></th>");
    let headingText = document.createTextNode(dayjs().set('date', 1).set('month', month).format('MMMM'));
    headingCell.append(headingText);
    headingRow.append(headingCell);
    calendarTable.append(headingRow);

    // creating all cells
    let date = 1;
    // creates table rows; there will never be more than 6 rows
    for (let i = 0; i < 6; i++) {
      let row = $("<tr></tr>");

      //creating individual cells, filing them up with data.
      for (let j = 0; j < 7; j++) {
        // Create blank cells at the beginning of the table
        if (i === 0 && j < firstDay) {
          let cell = document.createElement("td");
          let cellText = document.createTextNode("");
          cell.appendChild(cellText);
          row.append(cell);
        }
        // Stop creating cells at the end of the table
        else if (date > numberOfDaysInMonth && j === 0) {
          break;
        }
        // Add date cell to the table
        else {
          let cell = $(`<td></td>`);
          if (date <= numberOfDaysInMonth) {
            cell.append($(`<div class='date'>${date}</div>`));
          }
          // color today's date
          if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
            cell.addClass("today");
          }
          // color dates that match valid date list
          let matchingDateIndex = scheduledDates.findIndex((dateToCompare) => {
            return date === dateToCompare.date.date()
              && year === dateToCompare.date.year()
              && month === dateToCompare.date.month();
          });
          // Find days with scheduled times
          if (matchingDateIndex !== -1) {
            cell.addClass("scheduled");
            let matchingDate = scheduledDates[matchingDateIndex];
            let timeText = matchingDate.time.reduce((accumulator, currentTime) => {
              return accumulator === '' ? currentTime : `${accumulator}, ${currentTime}`;
            }, '');
            let cellContent = `
              <div class="scheduled-cell-content">
              <div class="Time"><span class="label">Times:</span> ${timeText}</div>
              <div class="aircraft"><span class="label">Aircraft:</span> ${matchingDate.aircraft}</div>
              <div class="destination"><span class="label">Dest:</span> ${matchingDate.destination}</div>
              <div class="instructor"><span class="label">Instr:</span> ${matchingDate.instructor}</div>
              </div>
            `;
            cell.append($(cellContent));
            cell.click(function() {
              return goToDate(matchingDate.date);
            })
          }
          row.append(cell);
          date++;
        }
      }
      calendarTable.append(row); // appending each row into calendar body.
    }
    return calendarTable;
  }

  // Create container DIV to contain calendar tables
  let calendarContainer = $('<div id="mini-calendars-container"></div>');
  $('#PrintHeading').after(calendarContainer);

  // Extract all data from the existing schedule table
  let allData = $('#fg').find('tr').map(function() {
    let $this = $(this);
    let returnObj = {
      date: dayjs($this.find('td:nth-child(1)').text()),
      time: [$this.find('td:nth-child(2)').text()],
      aircraft: $this.find('td:nth-child(4)').text(),
      destination: $this.find('td:nth-child(5)').text(),
      instructor: $this.find('td:nth-child(6)').text(),
    };
    return returnObj;
  }).get()
    .filter((data) => {
      return data.date.isValid();
    })
    .reduce(((accumulator, currentValue) => {
      let matchingDateIndex = accumulator.findIndex(value => {
        return currentValue.date.isSame(value.date);
      });
      if (matchingDateIndex !== -1) {
        accumulator[matchingDateIndex].time.push(currentValue.time[0]);
        return accumulator;
      }
      return [...accumulator, currentValue]
    }), []);

  // Find which months need calendars generated
  let monthsToShow = allData.reduce((accumulator, currentDate) => {
    let month = currentDate.date.month();
    if (accumulator.indexOf(month) === -1) {
      return [...accumulator, month];
    }
    return accumulator;
  }, []);

  monthsToShow.forEach((month) => {
    let newCalendar = createCalendar(month, 2021, allData);
    calendarContainer.append(newCalendar);
  });

  /***********************************
   * FINALLY...
   ***********************************/
  // Always highlight booked dates in calendar
  highlightAllBookedDates();

})();


/***********************************
* STYLES / CSS
***********************************/


let css = `

body {
  margin: 8px;
  background-color: #e8e8e8 !important;
}

#main-nav {
  margin: -8px -8px 20px;
  background: #4682b4;
  color: white;
  display: flex;
  font-size: 12px;
}
#main-nav a {
  color: white;
  line-height: 40px;
  margin: 0 20px;
}

table#TableHeader,
table#Table2 {
  background: white;
  /* border: 1px solid #c7c7c7; */
  border-radius: 8px;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 1px 1px 0 rgba(0, 0, 0, 0.14), 0 2px 1px -1px rgba(0, 0, 0, 0.12);
}
table#TableHeader {
  margin: 0 auto 20px;
}
table#Table2 {
  padding: 10px;
  margin: 20px auto;
}

table#TableHeader td {
  padding: 10px;
}

table.schedule {
  width: 700px;
}

.next-prev-buttons {
  margin: 10px auto -10px;
  width: 700px;
  display: flex;
  justify-content: space-between;
}
.next-prev-buttons button {
  padding: 10px 20px;
}

.hide-cells td.hidden-cell {
  /* opacity: 0.15; */
  display: none;
}
.schedule,
.schedule tr {
  background-color: transparent;
}
td[client="StoutR"] {
  background-color: #4ec225 !important;
}

.ST5 {
  /* Missing Permissions */
  background-color: #cc3333;
}
.ST7 {
  /* Rental Only */
  background-color: #497fb6;
}
.ST9 {
  /* Rental and Instructor */
  background-color: #1dbd8a;
}

#RadCalendar1_Top td[client="StoutR"] {
  background: #4ec225;
}
#RadCalendar1_Top td[client="StoutR"].rcSelected {
  background: #317817 !important;
}

#Table2 > tbody > tr:nth-child(1) > td:nth-child(1) {
  background: transparent !important;
}
#Table2 > tbody > tr:nth-child(1) > td:nth-child(1) > div:nth-child(2) {
  color: #696969;
  font-size: 1.7em;
  padding: 0.5em;
  border: 1px solid #868686;
  border-radius: 5px;
  margin-top: 6px;
  height: auto !important;
  width: auto !important;
}
#Table2 > tbody > tr:nth-child(1) > td:nth-child(1) > div:nth-child(2):hover {
  background: #eee;
}
.RadCalendar_Default .rcMain .rcOtherMonth,
.RadCalendar_Default .rcRow .rcHover {
  font-size: xx-small;
}

#mini-calendars-container {
  margin: 10px auto 20px;
  max-width: 1000px;
}
#mini-calendars-container table {
  border-collapse: collapse;
  width: 100%;
  margin-bottom: 20px;
  table-layout: fixed;
}
#mini-calendars-container table td {
  border: 1px solid gray;
  background: white;
  height: 100px;
  vertical-align: top;
}
#mini-calendars-container table td.today {
  background: #adadad;
}
#mini-calendars-container table td.scheduled {
  background: #c3f0b2;
  cursor: pointer;
}
#mini-calendars-container table td.scheduled.today {
  background: #83e061;
}
#mini-calendars-container table td .date {
  text-align: right;
  font-weight: bold;
  border-bottom: 1px solid #888;
  padding: 4px 7px;
  margin: -1px;
  background-color: #c8daea;
}
#mini-calendars-container .scheduled-cell-content {
  padding: 7px;
  font-size: 12px;
}
#mini-calendars-container .scheduled-cell-content .label {
  font-weight: bold;
}

#Table2 > tbody > tr:nth-child(1) > td:nth-child(7) span,
td.ScheduleKey span {
    padding: 6px 0;
}

`;
GM_addStyle(css);

